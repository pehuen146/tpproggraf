struct VS_INPUT
{
	float4 position : POSITION;
	float3 normal : NORMAL;
	float2 uv : TEXCOORD;
};

struct VS_OUTPUT
{
	float4 position : SV_POSITION;
	float2 uv : TEXCOORD;
	float3 normal : TEXCOORD2;
	float diffuse : TEXCOORD3;
};

float3 lightDir;
float4 lightCol;
float4 ambientCol;
float4x4 matMvp;
float4x4 matRot;

sampler2D tex : register(s0);

VS_OUTPUT VS(VS_INPUT vertex)
{
	VS_OUTPUT fragment;
	fragment.position = mul(vertex.position, matMvp);
	fragment.uv = vertex.uv;
	fragment.normal = mul(vertex.normal, matRot);

	//Calculo por vertices
	fragment.diffuse = max(0, dot(fragment.normal, lightDir));
	return fragment;
}


float4 PS(VS_OUTPUT fragment) : COLOR
{
	//Calculo por pixeles
	//fragment.diffuse = max(0, dot(fragment.normal, lightDir));

	//Efecto toon b�sico
	/*if (fragment.diffuse > 0.5)
	fragment.diffuse = 1;
	else
	fragment.diffuse = 0;*/

	float4 color = tex2D(tex, fragment.uv);
	return (ambientCol + fragment.diffuse * lightCol) * color;
}

technique Efecto
{
	pass p0
	{
		VertexShader = compile vs_2_0 VS();
		PixelShader = compile ps_2_0 PS();
	}
}