#ifndef MYGAME_H
#define MYGAME_H

#include "Game.h"
class MyGame : public Game
{
public:
	int cont = 0;
	void LoadContent(LPDIRECT3DDEVICE9 dev) override;
	void Update(LPDIRECT3DDEVICE9 dev) override;
	~MyGame();
};

#endif // !MYGAME_H