// Juego.cpp: define el punto de entrada de la aplicación.
//

#include "stdafx.h"
#include "Juego.h"
#include "MyGame.h"



int APIENTRY _tWinMain(_In_     HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_     LPTSTR    lpCmdLine,
	_In_     int       nCmdShow)
{
	MyGame jueguito;

	AllocConsole();				  // para abrir consola
	freopen("CONOUT$", "w", stdout);// para abrir consola

	jueguito.Run(hInstance, nCmdShow);

	return 0;
}