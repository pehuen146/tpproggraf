#include "stdafx.h"
#include "MyGame.h"
#include "Model.h"
#include "Composite.h"
#include "Transform.h"
#include "Material.h"
#include "MeshRenderer.h"
#include "AnimatedMeshRenderer.h"
#include "TilemapData.h"
#include "Frustum.h"
#include "SceneImporter.h"
#include <iostream>

#include <assimp\cimport.h>
#include <assimp\scene.h>
#include <assimp\postprocess.h>
#include <vld.h>

Composite* objeto;

void MyGame::LoadContent(LPDIRECT3DDEVICE9 dev)
{
	//genji->LoadShader(dev, L"Assets/efecto.fx");

	dev->SetRenderState(D3DRS_ZWRITEENABLE, true);
	dev->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESS);
	dev->SetRenderState(D3DRS_ZENABLE, true);

	m_Input->inputMap["Left"] = new vector<int>{ DIK_LEFTARROW };
	m_Input->inputMap["Right"] = new vector<int>{ DIK_RIGHTARROW };
	m_Input->inputMap["Up"] = new vector<int>{ DIK_UPARROW };
	m_Input->inputMap["Down"] = new vector<int>{ DIK_DOWNARROW };

	m_Input->inputMap["WASDLeft"] = new vector<int>{ DIK_A };
	m_Input->inputMap["WASDRight"] = new vector<int>{ DIK_D };
	m_Input->inputMap["WASDUp"] = new vector<int>{ DIK_W };
	m_Input->inputMap["WASDDown"] = new vector<int>{ DIK_S };

	m_Input->inputMap["E"] = new vector<int>{ DIK_E };
	m_Input->inputMap["Q"] = new vector<int>{ DIK_Q };
	
	camera->isometricView = false;
	camera->View(dev, 60, 640, 480, 0.1f, 30);

	SceneImporter sI;

	sI.Import("Assets/bspTest4.dae", root, dev);
	
	bsp->AddPlaneOrObject(root);

	objeto = root->findChild("Cube");

	camera->m_Transform->SetPos(D3DXVECTOR3(0, 1, -10));
	camera->View(dev, 60, 640, 480, 0.1f, 30);

	dev->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
}

void MyGame::Update(LPDIRECT3DDEVICE9 dev)
{
	if (m_Input->GetKey("Left"))
		objeto->GetComponent<Transform>()->Move(D3DXVECTOR3(-0.1f, 0, 0));

	if (m_Input->GetKey("Right"))
		objeto->GetComponent<Transform>()->Move(D3DXVECTOR3(0.1f, 0, 0));

	if (m_Input->GetKey("Up"))
		objeto->GetComponent<Transform>()->Move(D3DXVECTOR3(0, 0, 0.1f));

	if (m_Input->GetKey("Down"))
		objeto->GetComponent<Transform>()->Move(D3DXVECTOR3(0, 0, -0.1f));

	

	if (m_Input->GetKey("WASDLeft")) 
	{
		camera->m_Transform->Move(D3DXVECTOR3(-0.1f,0 , 0));
		camera->View(dev, 60, 640, 480, 0.1f, 30);
	}

	if (m_Input->GetKey("WASDRight"))
	{
		camera->m_Transform->Move(D3DXVECTOR3(0.1f,0, 0));
		camera->View(dev, 60, 640, 480, 0.1f, 30);
	}

	if (m_Input->GetKey("WASDUp")) 
	{
		camera->m_Transform->Move(D3DXVECTOR3(0, 0, 0.1f));
		camera->View(dev, 60, 640, 480, 0.1f, 30);
	}

	if (m_Input->GetKey("WASDDown")) 
	{
		camera->m_Transform->Move(D3DXVECTOR3(0, 0, -0.1f));
		camera->View(dev, 60, 640, 480, 0.1f, 30);
	}

	if (m_Input->GetKey("E"))
	{
		camera->m_Transform->Rotate(D3DXVECTOR3(0, -100.0f, 0));
		camera->View(dev, 60, 640, 480, 0.1f, 30);
	}

	if (m_Input->GetKey("Q"))
	{
		camera->m_Transform->Rotate(D3DXVECTOR3(0, 100.0f, 0));
		camera->View(dev, 60, 640, 480, 0.1f, 30);
	}

	frustum->UpdatePlanes(camera->viewMatrix * camera->projMatrix);

	/*D3DXMATRIX mvp = transform->GetModelMatrix()* camera.viewMatrix * camera.projMatrix;

	D3DXVECTOR4 lightDir(0, 0, 1, 0);
	D3DXVECTOR4 lightCol(1, 1, 0, 0);
	D3DXVECTOR4 ambientCol(0.7f, 0, 0, 0);

	if (genji->GetShader())
	{
		genji->GetShader()->SetVector("ambientCol", &ambientCol);
		genji->GetShader()->SetVector("lightDir", &lightDir);
		genji->GetShader()->SetVector("lightCol", &lightCol);
		genji->GetShader()->SetMatrix("matRot", &transform->GetRotMatrix());
		genji->GetShader()->SetMatrix("matMvp", &mvp);
	}*/
}

MyGame::~MyGame()
{


	vector<Component*> components = root->GetComponents();

	for (unsigned int i = 0; i < components.size(); i++)
	{
		components.at(i) = NULL;
		delete components.at(i);
	}
}
