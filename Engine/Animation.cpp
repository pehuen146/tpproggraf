#include "stdafx.h"
#include "Animation.h"

Animation::Animation(string _animationName, vector<int> _animationIndexes, float _velocity)
{
	animationName = _animationName;
	animationIndexes = _animationIndexes;
	velocity = _velocity;
}
