#ifndef GAME_H
#define GAME_H


#include "stdafx.h"
#include <d3d9.h> //Busca el header de directx en los path
#pragma comment (lib, "d3d9.lib") //Incluyo la lib a mi proyecto
#include "EngineApi.h"
#include "Input.h"
#include "Composite.h"
#include "Camera.h"
#include "Tilemap.h"
#include "Time.h"
#include "Frustum.h"
#include "BSP.h"
#include <list>

class ENGINE_API Game
{
protected:
	Composite* root=new Composite();
	Camera* camera = new Camera();
	BSP* bsp = new BSP();
	list<Composite*> objectsToDraw;
public:
	vector<Composite*> inTreeBSP;
	bool ortographicView=true;
	Game();
	~Game();
	virtual void LoadContent(LPDIRECT3DDEVICE9 dev) = 0;
	virtual void Update(LPDIRECT3DDEVICE9 dev)=0;
	void Run(HINSTANCE hInstance, int nCmdShow);
	Input* m_Input;
	Time* m_Time=new Time();
	Frustum* frustum = new Frustum();
};

#endif // !GAME_H