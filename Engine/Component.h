#ifndef COMPONENT_H
#define COMPONENT_H

#include <d3d9.h> //Busca el header de directx en los path
#pragma comment (lib, "d3d9.lib") //Incluyo la lib a mi proyecto
#include <d3dx9.h> //Busca el header de directx en los path
#pragma comment (lib, "d3dx9.lib") //Incluyo la lib a mi proyecto
#include "EngineApi.h"
#include "Model.h"
#include "Frustum.h"	

class ENGINE_API Component
{
	class Composite* parent;
public:
	Component();
	~Component();
	virtual void Update();
	virtual void Dibujar(LPDIRECT3DDEVICE9 dev, Frustum* frustum);
	void SetParent(Composite* parent);
	Composite* GetParent();
};

#endif // !COMPONENT_H