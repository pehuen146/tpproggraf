#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "Component.h"
class ENGINE_API Transform : public Component
{
private:
	D3DXVECTOR3 _pos;
	D3DXVECTOR3 _rot;
	D3DXVECTOR3 _sca;
	D3DXMATRIX localModelMatrix;
	D3DXMATRIX worldMatrix;
public:
	D3DXVECTOR3 GetPos();
	D3DXVECTOR3 GetRot();
	D3DXVECTOR3 GetSca();
	void SetPos(D3DXVECTOR3 pos);
	void SetPos(float x, float y, float z);
	void SetRot(D3DXVECTOR3 rot);
	void SetRot(float x, float y, float z);
	void SetSca(D3DXVECTOR3 sca);
	void SetSca(float x, float y, float z);
	D3DXMATRIX GetWorldMatrix();
	void CalculateModelMatrix();
	void CalculateWorldMatrix(D3DXMATRIX parentMatrix);
	void NoParentModelMatrix();
	void Move(D3DXVECTOR3 movement);
	void Rotate(D3DXVECTOR3 rotation);
	void Scale(D3DXVECTOR3 scale);
	void updateTransBB();
	Transform();
	~Transform();
	
};

#endif // !TRANSFORM_H