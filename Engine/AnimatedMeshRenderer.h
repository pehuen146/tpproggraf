#ifndef ANIMATEDMESHRENDERER_H
#define ANIMATEDMESHRENDERER_H

#include "MeshRenderer.h"
#include "EngineApi.h"
#include "Animation.h"
#include "Time.h"
#include <vector>
using namespace std;

class ENGINE_API AnimatedMeshRenderer : public MeshRenderer
{
	int counter = 0;
	vector<Animation> animations;
	Animation* currentAnimation = NULL;
	float defaultAnimationVelocity = 20.0f;
public:
	Time* time;
	float sumador = 0;
	AnimatedMeshRenderer(LPDIRECT3DDEVICE9 dev, Time* _time, int _tileXAmount, int _tileYAmount, Model* _model, Material* _material);
	int animationXSelector = 0;
	int animationYSelector = 0;
	int tileXAmount;
	int tileYAmount;
	vector<LPDIRECT3DVERTEXBUFFER9> frames;
	void AddAnimation(Animation animation);
	void Play(string animationName);
	void DibujarMesh(LPDIRECT3DDEVICE9 dev) override;
	string GetCurrentAnimationName();
};

#endif // !ANIMATEDMESHRENDERER