#pragma once
#include <chrono>
using namespace std::chrono;

class Time
{
	int lastFrameMs;
public:
	float deltaTime;
	int getLastFrameMs();
	void setLastFrameMs(int _lastFrameMs);
	int getMs();
};

