#include "stdafx.h"
#include "Transform.h"
#include "Composite.h"


Transform::Transform()
{
	_pos = D3DXVECTOR3(0, 0, 0);
	_sca = D3DXVECTOR3(1, 1, 1);
	_rot = D3DXVECTOR3(0, 0, 0);
}


Transform::~Transform()
{
}

D3DXVECTOR3 Transform::GetPos()
{
	return _pos;
}

D3DXVECTOR3 Transform::GetRot()
{
	return _rot;
}

D3DXVECTOR3 Transform::GetSca()
{
	return _sca;
}

void Transform::SetPos(D3DXVECTOR3 pos)
{
	_pos = pos;
}

void Transform::SetPos(float x, float y, float z)
{
	_pos = D3DXVECTOR3(x, y, z);
}

void Transform::SetRot(D3DXVECTOR3 rot)
{
	//_rot = D3DXVECTOR3(D3DXToRadian(rot.x), D3DXToRadian(rot.y), D3DXToRadian(rot.z));
	_rot = rot;
}

void Transform::SetRot(float x, float y, float z)
{
	//_rot = D3DXVECTOR3(D3DXToRadian(x), D3DXToRadian(y), D3DXToRadian(z));
	_rot = D3DXVECTOR3(x, y, z);
}

void Transform::SetSca(D3DXVECTOR3 sca)
{
	_sca = sca;
}

void Transform::SetSca(float x, float y, float z)
{
	_sca = D3DXVECTOR3(x, y, z);
}

D3DXMATRIX Transform::GetWorldMatrix()
{
	return worldMatrix;
}

void Transform::CalculateModelMatrix()
{
	D3DXMATRIX transMat;
	D3DXMATRIX scaMat;
	D3DXMATRIX rotXMat;
	D3DXMATRIX rotYMat;
	D3DXMATRIX rotZMat;

	D3DXMatrixTranslation(&transMat, _pos.x, _pos.y, _pos.z);
	D3DXMatrixScaling(&scaMat, _sca.x, _sca.y, _sca.z);
	D3DXMatrixRotationX(&rotXMat, _rot.x);
	D3DXMatrixRotationY(&rotYMat, _rot.y);
	D3DXMatrixRotationZ(&rotZMat, _rot.z);

	localModelMatrix = scaMat * rotXMat * rotYMat * rotZMat * transMat;
}

void Transform::CalculateWorldMatrix(D3DXMATRIX parentMatrix)
{
	worldMatrix = localModelMatrix * parentMatrix;
}

void Transform::NoParentModelMatrix()
{
	worldMatrix = localModelMatrix;
}

void Transform::Move(D3DXVECTOR3 movement)
{
	_pos += movement;
}

void Transform::Rotate(D3DXVECTOR3 rotation)
{
	_rot += D3DXVECTOR3(D3DXToRadian(rotation.x), D3DXToRadian(rotation.y), D3DXToRadian(rotation.z));
}

void Transform::Scale(D3DXVECTOR3 scale) 
{
	_sca += scale;
}

void Transform::updateTransBB()
{
	GetParent()->updateBoundingBox();
}

