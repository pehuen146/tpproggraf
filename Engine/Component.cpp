#include "stdafx.h"
#include "Component.h"


Component::Component()
{
}


Component::~Component()
{
}

void Component::Update()
{
}

void Component::Dibujar(LPDIRECT3DDEVICE9 dev, Frustum* frustum)
{
}

void Component::SetParent(Composite * parent)
{
	this->parent = parent;
}

Composite * Component::GetParent()
{
	return parent;
}

