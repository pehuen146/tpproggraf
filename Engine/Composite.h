#ifndef COMPOSITE_H
#define COMPOSITE_H

#include <vector>
#include "Component.h"
#include "EngineApi.h"
#include "BoundingBox.h"
#include "Frustum.h"

#include <iostream>

using namespace std;

class ENGINE_API Composite : public Component
{
private:
	template<class T> void GetComponentsInParent(vector<T*>*);
	vector<Component*> components;
	string name;
protected:
	virtual void UpdateComposite();
	BoundingBox transBB;
public:
	void Add(Component* component);
	void Remove(Component* component);
	void Update() override final;
	void Dibujar(LPDIRECT3DDEVICE9 dev, Frustum* frustum) override;
	D3DXVECTOR3 getWorldPosition();
	D3DXVECTOR3 getWorldRotation();
	string GetName();
	void SetName(string _name);
	template<class T> T* GetComponent();
	template<class T> T* GetComponentInChildren();
	template<class T> T* GetComponentInParent();
	vector<Component*> GetComponents();
	template<class T> vector<T*>* GetComponentsInParent();
	BoundingBox getTransBB();
	void setTransBB(BoundingBox _transBB);
	void updateBoundingBox();
	Composite* findChild(string childName);
};


template<class T>
inline T* Composite::GetComponent()
{
	for (size_t i = 0; i < components.size(); i++)
	{
		T* comp = dynamic_cast<T*>(components[i]);
		if (comp != nullptr) return comp;
	}
	return nullptr;
}

template<class T>
inline T * Composite::GetComponentInChildren()
{
	T* comp = dynamic_cast<T*>(this);
	if (comp) return comp;

	for (size_t i = 0; i < components.size(); i++)
	{
		Component* child = components[i];
		Composite* compositeChild = dynamic_cast<Composite*>(child);
		if (compositeChild)
		{
			T* childComp = compositeChild->GetComponentInChildren<T>();
			if (childComp) return childComp;
		}
		else
		{
			T* childComp = dynamic_cast<T*>(child);
			if (childComp) return childComp;
		}
	}

	return nullptr;
}

template<class T>
inline T * Composite::GetComponentInParent()
{
	T* comp = dynamic_cast<T*>(this);

	//Verifico si yo soy el tipo que esta buscando
	if (comp != nullptr)
	{
		return comp;
	}
	//Si no verifico si tengo padre para preguntarle a el
	else if (GetParent() != nullptr)
	{
		return GetParent()->GetComponentInParent<T>();
	}
	//Si no no esta lo que se esta buscando
	else
	{
		return nullptr;
	}
}

template<class T>
inline vector<T*>* Composite::GetComponentsInParent()
{
	vector<T*>* vec = new vector<T*>();
	GetComponentsInParent<T*>(vec);
	return vec;
}

template<class T>
inline void Composite::GetComponentsInParent(vector<T*>* vec)
{
	T* comp = dynamic_cast<T*>(this);
	if (comp) vec->push_back(comp);
	if (GetParent()) GetParent()->GetComponentsInParent(vec);
}

#endif // !COMPOSITE_H