#ifndef ANIMATION_H
#define ANIMATION_H
#include <string>
#include <vector>
#include "EngineApi.h"
using namespace std;

class ENGINE_API Animation
{
public:
	Animation(string _animationName, vector<int> _animationIndexes, float _velocity);
	string animationName;
	vector<int> animationIndexes;
	float velocity = NULL;
};

#endif // !ANIMATION_H_H