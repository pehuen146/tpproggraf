#ifndef FRUSTUM_H
#define FRUSTUM_H

#include "BoundingBox.h"

class ENGINE_API Frustum
{
	LPD3DXPLANE planes[6];
public:
	void UpdatePlanes(D3DXMATRIX viewProj);
	bool BoxInFrustum(BoundingBox bb);
	Frustum();
	~Frustum();
};
#endif

