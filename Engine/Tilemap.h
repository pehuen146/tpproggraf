#ifndef TILEMAP_H
#define TILEMAP_H

#include <vector>
#include "EngineApi.h"
#include "Transform.h"
#include <d3dx9.h> //Busca el header de directx en los path
#pragma comment (lib, "d3dx9.lib") //Incluyo la lib a mi proyecto
#include "Component.h"
#include "Model.h"
using namespace std;

class ENGINE_API Tilemap : public Component
{
private:
	LPDIRECT3DTEXTURE9 texture = NULL;
	vector<vector<vector<int>>> tileMap;
	int verticalTiles;
	int horizontalTiles;
	Model* m_Model;
public:
	Tilemap(LPCWSTR imageName, LPDIRECT3DDEVICE9 dev, int _verticalTiles,int _horizontalTiles);
	Transform* transform;
	void SetTilemap(vector<vector<vector<int>>> _tileMap);
	void Dibujar(LPDIRECT3DDEVICE9 dev, Frustum* frustum) override;
	void SetModel(Model* model);
};

#endif // !TILEMAP_H