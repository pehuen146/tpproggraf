#include "stdafx.h"
#include "AnimatedMeshRenderer.h"
#include "Vertex.h"
#include "Model.h"
#include "Transform.h"

AnimatedMeshRenderer::AnimatedMeshRenderer(LPDIRECT3DDEVICE9 dev, Time* _time, int _tileXAmount, int _tileYAmount, Model* _model, Material* _material) : MeshRenderer(_model, _material)
{
	time = _time;

	tileXAmount = _tileXAmount;
	tileYAmount = _tileYAmount;

	for (int i = 0; i < tileYAmount; i++)
	{
		for (int j = 0; j < tileXAmount; j++)
		{
			Vertex vertexes[] =
			{
				{ 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, (1.0f / tileXAmount)* animationXSelector,	   (1.0f / tileYAmount)* animationYSelector },
				{ 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, (1.0f / tileXAmount)*(animationXSelector + 1), (1.0f / tileYAmount)* animationYSelector },
				{ 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, (1.0f / tileXAmount)* animationXSelector ,	   (1.0f / tileYAmount)*(animationYSelector + 1) },
				{ 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, (1.0f / tileXAmount)*(animationXSelector + 1), (1.0f / tileYAmount)*(animationYSelector + 1) }
			};

			LPDIRECT3DVERTEXBUFFER9 vb;
			dev->CreateVertexBuffer(4 * sizeof(Vertex), 0, CUSTOMFVF, D3DPOOL_MANAGED, &vb, NULL);
			VOID* lockedData = NULL;
			vb->Lock(0, 0, (void**)&lockedData, 0);
			memcpy(lockedData, vertexes, sizeof(vertexes));
			vb->Unlock();

			frames.push_back(vb);

			animationXSelector++;
			if (animationXSelector >= tileXAmount)
				animationXSelector = 0;

		}
		animationYSelector++;
	}
}

void AnimatedMeshRenderer::AddAnimation(Animation animation)
{
	animations.push_back(animation);
}

void AnimatedMeshRenderer::Play(string animationName)
{
	for (int i = 0; i < animations.size(); i++)
	{
		if (animations.at(i).animationName == animationName)
		{
			currentAnimation = &animations.at(i);
		}
	}
}

void AnimatedMeshRenderer::DibujarMesh(LPDIRECT3DDEVICE9 dev)
{
	//Especificamos el formato del vertice
	dev->SetFVF(CUSTOMFVF);

	if (currentAnimation)
		sumador += currentAnimation->velocity*time->deltaTime;
	else
		sumador += defaultAnimationVelocity*time->deltaTime;

	counter = (int)sumador;

	if (currentAnimation)
	{

		if (counter >= currentAnimation->animationIndexes.size())
		{
			sumador = 0;
			counter = 0;
		}

		dev->SetStreamSource(0, frames.at(currentAnimation->animationIndexes.at(counter)), 0, sizeof(Vertex));
	}
	else
	{
		if (counter >= tileXAmount*tileYAmount)
		{
			sumador = 0;
			counter = 0;
		}

		dev->SetStreamSource(0, frames.at(counter), 0, sizeof(Vertex));
	}

	//Especifivamos cuales indices vamos a usar
	dev->SetIndices(m_Model->GetIndexBuffer());

	

	Transform *transform = GetParent()->GetComponent<Transform>();

	m_Material->SetBlend(dev, m_Material->getBlend());
	m_Material->setWrap(dev, m_Material->getWrap());
	m_Material->setFiltro(dev, m_Material->getFiltro());

	dev->SetTexture(0, m_Material->textura);

	m_Material->setFiltro(dev, 1);

	m_Material->setWrap(dev, 1);

	dev->SetTransform(D3DTS_WORLD, &transform->GetWorldMatrix());

	dev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);
}

string AnimatedMeshRenderer::GetCurrentAnimationName()
{
	return currentAnimation->animationName;
}
