#include "stdafx.h"
#include "Time.h"

int Time::getLastFrameMs()
{
	return lastFrameMs;
}

void Time::setLastFrameMs(int _lastFrameMs)
{
	lastFrameMs = _lastFrameMs;
}



int Time::getMs()
{
	time_point<system_clock> actualTime = system_clock::now();
	system_clock::duration duration = actualTime.time_since_epoch();
	milliseconds ms = duration_cast<milliseconds>(duration);
	return ms.count();
}
