#include "stdafx.h"
#include "Tilemap.h"

Tilemap::Tilemap(LPCWSTR imageName, LPDIRECT3DDEVICE9 dev, int _verticalTiles, int _horizontalTiles)
{
	D3DXCreateTextureFromFile(dev, imageName, &texture);

	dev->SetTexture(0, texture);

	verticalTiles = _verticalTiles;
	horizontalTiles = _horizontalTiles;
}

void Tilemap::SetTilemap(vector<vector<vector<int>>> _tileMap)
{
	tileMap = _tileMap;
}

void Tilemap::Dibujar(LPDIRECT3DDEVICE9 dev,Frustum* frustum)
{
	dev->SetFVF(CUSTOMFVF);
	dev->SetStreamSource(0, m_Model->GetVertexBuffer(), 0, sizeof(Vertex));
	dev->SetIndices(m_Model->GetIndexBuffer());

	float anchoTile = transform->GetSca().x;
	float altoTile = transform->GetSca().y;

	D3DXMATRIX matSca;
	D3DXMatrixScaling(&matSca, anchoTile, altoTile, 1);
	for (int indiceCapa = 0; indiceCapa < tileMap.size(); indiceCapa++)
	{
		vector<vector<int>>* capa = &tileMap[indiceCapa];

		for (int indiceFila = 0; indiceFila < capa->size(); indiceFila++)
		{
			vector<int>* fila = &capa->at(indiceFila);

			for (int indiceColumna = 0; indiceColumna < fila->size(); indiceColumna++)
			{
				int celda = fila->at(indiceColumna);
				if (celda != -1)
				{
					float posX = (anchoTile * indiceColumna) + transform->GetPos().x;
					float posY = (altoTile * indiceFila) + transform->GetPos().y;


					int col = celda / horizontalTiles;

					D3DXMATRIX transMat2;
					D3DXMatrixIdentity(&transMat2);
					transMat2._31 = (1.0f / horizontalTiles)*celda;
					transMat2._32 = (1.0f / verticalTiles)*col;

					D3DXMATRIX matScale;
					D3DXMatrixScaling(&matScale, 1.0f / horizontalTiles, 1.0f / verticalTiles, 1.0f);


					D3DXMATRIX model = matScale * transMat2;

					dev->SetTextureStageState(0, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_COUNT2);

					dev->SetTransform(D3DTS_TEXTURE0, &model);
					dev->SetTexture(0, texture);

					//alpha blending


					D3DXMATRIX matTrans;
					D3DXMatrixTranslation(&matTrans, posX, -posY, transform->GetPos().z);

					D3DXMATRIX mat = matSca * matTrans;
					dev->SetTransform(D3DTS_WORLD, &mat);

					dev->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
					dev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
					dev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

					dev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);

					D3DXMatrixScaling(&matScale, 1, 1, 1);
					D3DXMatrixTranslation(&transMat2, 0, 0, 0);
					model = matScale * transMat2;
					dev->SetTransform(D3DTS_TEXTURE0, &model);
				}
			}
		}
	}
}

void Tilemap::SetModel(Model* model)
{
	m_Model = model;
}
